#ifndef MENU_H
#define MENU_H

#include "personmanager.h"

template <class S, class T> class Menu
{
private :
    List<T> list;
    S * parent;
    int selection;

public :
    Menu();
    Menu(List<T> & l);
    Menu(List<T> & l, S * p);

    void display();
    void browse(int amount);
    const T getSelection();
    void getAroundSelection(uint16_t n, List<T> & l);
    void accept();
    void goBack();
    void setList(List<T> & l);
    void setParent(S * p);
};

#endif // MENU_H