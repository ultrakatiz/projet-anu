#include "menu.h"

template <class S, class T>
Menu<S, T>::Menu() :
parent(nullptr), selection(0)
{}

template <>
Menu<nullptr_t, Person *>::Menu() :
parent(nullptr), selection(0)
{}

template <>
Menu<Menu<nullptr_t, Person *>, int>::Menu() :
parent(nullptr), selection(0)
{}

template <class S, class T>
Menu<S, T>::Menu(List<T> & l) :
list(l), parent(nullptr), selection(0)
{}

template <class S, class T>
Menu<S, T>::Menu(List<T> & l, S * p) :
list(l), parent(p), selection(0)
{}

template <class S, class T>
void Menu<S, T>::display()
{
    // TODO
}

template <class S, class T>
void Menu<S, T>::browse(int amount)
{
    if (selection + amount >= static_cast<int>(list.getSize()))
        selection += amount - static_cast<int>(list.getSize());
    else
    {
        if (selection + amount < 0)
            selection += amount + static_cast<int>(list.getSize());
        else
            selection += amount;
    }
}

template <class S, class T>
const T Menu<S, T>::getSelection()
{
    return list[selection];
}

template <class S, class T>
void Menu<S, T>::accept()
{
    // TODO
}

template <class S, class T>
void Menu<S, T>::goBack()
{
    // TODO
}

template <class S, class T>
void Menu<S, T>::setParent(S * p)
{
    parent = p;
}

template <class S, class T>
void Menu<S, T>::setList(List<T> & l)
{
    list = l;
}

template <class S, class T>
void Menu<S, T>::getAroundSelection(uint16_t n, List<T> & l)
{
    Person p;

    if (list.getSize() <= 0) return;

    for (uint16_t i=0; i<2*n+1; i++)
    {
        if (selection - n + i < 0)
            p = list.at(selection - n + list.getSize() + i);
        else
        {
            if (selection - n + i >= list.getSize())
                p = list.at(selection - n + i - list.getSize());
            else
                p = list.at(selection - n + i);
        }
        l->add(p);
    }
    return &l;
}