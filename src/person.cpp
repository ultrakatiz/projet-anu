#include "person.h"

Person::Person() : money(0), name("nobody") {}
Person::Person(char n[8]) : money(0)
{
    for (uint8_t i=0; i<8; i++)
        name[i] = n[i];
}
Person::Person(char n[8], int m) : money(m)
{
    for (uint8_t i=0; i<8; i++)
        name[i] = n[i];
}

Person::Person(const Person & p)
{
    *this = p;
}

Person::~Person()
{}

const int Person::getMoney()
{
    return money;
}

void Person::setMoney(int amount)
{
    money = amount;
}

void Person::addMoney(int amount)
{
    money += amount;
}

void Person::operator = (const Person & p)
{
    *this = p;
}

bool Person::operator < (const Person & p) const
{
    return name < p.name;
}

bool Person::operator == (const Person & p) const
{
    return name == p.name;
}