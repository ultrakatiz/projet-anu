#ifndef TEXTMATRIX_H
#define TEXTMATRIX_H

#include <Arduino.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 8
#define MAX_MATRIX_LENGTH 24
#define LETTER_SPACE 3

class TextMatrix
{
private :
    char text[8];

    void reset();
    void setLetter(uint8_t n);

    uint8_t letter;
    bool arr[5][3];

public :
    int matrix[5][MAX_MATRIX_LENGTH];

    TextMatrix();
    TextMatrix(char s[8]);
    TextMatrix(TextMatrix & tm);
    ~TextMatrix();

    void setText(char s[8]);
    String getText();
};

#endif // TEXTMATRIX_H