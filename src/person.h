#ifndef PERSON_H
#define PERSON_H

#include <Arduino.h>
#include <stdlib.h>

class Person
{
private :
    int16_t money;

public :
    char name[8];

    Person();
    Person(char n[8]);
    Person(char n[8], int m);
    ~Person();
    Person(const Person &);

    const int getMoney();
    void setMoney(int amount);
    void addMoney(int amount);

    void operator = (const Person & p);
    bool operator < (const Person & p) const;
    bool operator == (const Person & p) const;
};

#endif // PERSON_H