#ifndef PERSONMANAGER_H
#define PERSONMANAGER_H

#include <EEPROM.h>
#include "list.h"

#define MAX_SIZE 100

class PersonManager
{
private :
    Person * persons;
    uint8_t size;
    
public :
    PersonManager();
    ~PersonManager();
    PersonManager(PersonManager &) = delete;

    void add(String n, int m);
    void remove(Person & p);
    const Person & at(uint8_t i);
    const uint8_t getSize();

    void save();
    void load();
};

#endif // PERSONMANAGER_H