#ifndef LIST_H
#define LIST_H

#include "person.h"

#define MAX_LIST_SIZE 5

template <class T> class List
{
private :
    T list[MAX_LIST_SIZE];

public :
    List();
    List(List<T> & l);
    ~List();

    uint8_t size;

    void add(T elt);
    void remove(T elt);
    void removeAt(int n);

    T at(uint8_t i);
    T operator [] (uint8_t i);
};

#endif // LIST_H