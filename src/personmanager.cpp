#include "personmanager.h"

PersonManager::PersonManager()
{
    persons = new Person[MAX_SIZE];
}

PersonManager::~PersonManager()
{
    free(persons);
}

void PersonManager::add(String n, int m)
{
    if (size > MAX_SIZE) return;

    for (uint8_t i=0; i<8; i++)
    {
        if (i < n.length())
            persons[size].name[i] = n[i];
        else
            persons[size].name[i] = '\0';
    }
    persons[size].setMoney(m);

    Person tmp;

    for (uint8_t i=size; i>0; i--)
    {
        if (persons[i-1] < persons[i]) break;

        tmp = persons[i];
        persons[i] = persons[i-1];
        persons[i-1] = tmp;
    }
    size++;
}

void PersonManager::remove(Person & p)
{
    if (size <= 0) return;
    
    size--;

    for (int i=0; i<size; i++)
    {
        if (persons[i] < p) continue;

        persons[i] = persons[i+1];
    }
}

const Person & PersonManager::at(uint8_t i)
{
    return persons[i];
}

const uint8_t PersonManager::getSize()
{
    return size;
}

void PersonManager::save()
{
    int cursor = 0;
    EEPROM.put<uint16_t>(cursor, size);
    cursor += sizeof(uint16_t);

    for (uint16_t i=0; i<size; i++)
    {
        Person p = persons[i];
        
        for (int j=0; j<8; j++)
        {
            EEPROM.put<char>(cursor, p.name[j]);
            cursor += sizeof(char);
        }
        EEPROM.put<int>(cursor, p.getMoney());
        cursor += sizeof(int);
    }
}

void PersonManager::load()
{
    int cursor = 0;
    EEPROM.get<uint8_t>(cursor, size);
    cursor += sizeof(uint8_t);

    for (uint16_t i=0; i<size; i++)
    {
        Person p = persons[i];

        for (int j=0; j<8; j++)
        {
            EEPROM.get<char>(cursor, p.name[j]);
            cursor += sizeof(char);
        }
        int money;
        EEPROM.get<int>(cursor, money);
        p.setMoney(money);
        cursor += sizeof(int);
    }
}