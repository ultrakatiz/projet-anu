#include "list.h"

template <>
List<int>::List() :
size(0)
{}

template <>
List<Person *>::List() :
size(0)
{
    for (uint8_t i=0; i<MAX_LIST_SIZE; i++)
        list[i] = new Person();
}

template <>
List<Person *>::List(List<Person *> & l)
{
    for (uint8_t i=0; i<l.size; i++)
        list[i] = l.list[i];
    size = l.size;
}

template <>
List<Person *>::~List()
{
    for (uint8_t i=0; i<MAX_LIST_SIZE; i++)
        delete list[i];
}

template <>
List<int>::~List()
{}

template <>
Person * List<Person *>::at(uint8_t i)
{
    return list[i];
}

template <>
Person * List<Person *>::operator [] (uint8_t i)
{
    return list[i];
}

template <>
void List<Person *>::add(Person * elt)
{
    if (size >= MAX_LIST_SIZE) return;

    Person * tmp;
    list[size] = elt;

    for (uint8_t i=size; i>0; i--)
    {
        if (list[i-1] < list[i]) break;

        tmp = list[i];
        list[i] = list[i-1];
        list[i-1] = tmp;
    }
    size++;
}

template <>
void List<Person *>::remove(Person * elt)
{
    if (size <= 0) return;
    
    size--;

    for (int i=0; i<size; i++)
    {
        if (list[i] < elt) continue;

        list[i] = list[i+1];
    }
}

template <>
void List<Person *>::removeAt(int n)
{
    if (n < 0 || n >= size) return;

    size--;

    for (int i=n; i<size; i++)
    {
        list[i] = list[i+1];
    }
}
