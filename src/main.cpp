#include <Arduino.h>
#include <SPI.h>
#include <RGBmatrixPanel.h>
#include <EEPROM.h>

// Memory access with EEPROM
// Can stock up to 100 persons
// first 16 bytes indicates which blocks (8 bytes) are taken (0) or free (1)
// next 1 byte indicates the number of the first block in alphabetical order
// next 1 byte indicates the number of the last block in alphabetical order
// A block is :
// 4 bytes for the name
// 2 bytes for the money
// 1 byte for previous address in alphabetical order
// 1 byte for next address in alphabetical order

#pragma region DATA

  // Definitions for the RGB matrix
  #pragma region MATRIX

    #define CLK  8
    #define OE   9
    #define LAT 10
    #define A   A0
    #define B   A1
    #define C   A2
    #define D   A3

    RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false);

  #pragma endregion MATRIX

  // Definitions for Persons management in flash and ROM memory
  #pragma region PERSON

    #define MAX_SIZE 5
    #define NAME_SIZE 6

    uint32_t tmpName;
    int tmpMoney;
    uint8_t sz = 0;
    uint8_t selectP; // Person selected in the menu
    uint8_t selectM; // Amount of money selected in the menu
    uint8_t personCursor; // Block number of a person
    uint16_t memCursor; // Memory index of a person

    // Temporary char array, which is used to convert chars into an uint32_t
    char * chars = new char[7];

  #pragma endregion PERSON

  // Definitions for the Joystick
  #pragma region JOYSTICK

    #define JOYSTICK_MAX 1024

    #define yPin      A4
    #define xPin      A5
    #define buttonPin 11

  #pragma endregion JOYSTICK

  // Definitions for the Buttons
  #pragma region BUTTONS

    #define IS_PRESSED LOW

    #define okPin   12 // OK (yellow) button
    #define backPin 13 // BACK (red) button

  #pragma endregion BUTTONS

  // General definitions
  #pragma region GENERAL

    // Diffeerent menus
    #define MENU_MAIN   0
    #define MENU_PERSON 1
    #define MENU_CASH   2
    #define MENU_ADD    3
    #define MENU_REMOVE 4
    #define MENU_LOCK   5

    // Memory definitions
    #define BLOCK_SIZE 8  // Size of a Person block (explained up there)
    #define MEM_OFFSET 18 // Index of the first block in memory

    // Menu variables, used to update the screen only when necessary
    bool listChanged;
    bool selectionChanged;
    bool menuChanged;
    uint8_t currentMenu;

    // Selection variables
    uint8_t selectMenu;
    uint8_t selectL;

    // Letters selected in the ADD menu
    uint8_t letterCursors[6];

    // 3 primary colors used in the applications
    uint16_t baseColor;
    uint16_t selectColor;
    uint16_t otherColor;

    // Variables for the lock screen
    uint8_t unlock;
    uint8_t lastInput;

  #pragma endregion GENERAL

#pragma endregion DATA

// Functions used in the app
#pragma region FUNCTIONS

  // Put the chars into an int of 32 bits, 5 bits at a time
  uint32_t charsToInt(char * str)
  {
    uint32_t n = 0;
    uint32_t pow = 1073741824; // (2^5)^6

    for (uint8_t i=0; i<6; i++)
    {
      if (str[i] == '\0') break;
      pow /= 32; // 2^5
      n += pow * (str[i] - 'a' + 1);
    }
    return n;
  }

  // Returns the indexth letter of a compressed char list
  char letter(uint32_t n, uint8_t index)
  {
    char num = 0;

    for (uint8_t i=0; i<5; i++)
      bitWrite(num, i, bitRead(n, 5*(5 - index) + i));
    for (uint8_t i=5; i<8; i++)
      bitWrite(num, i, 0);

    if (num != 0) return num + 'a' - 1;
    return '\0';
  }

  // Returns a 16-bit int that represents a matrix of pixels
  // corresponding to a given character
  uint16_t charToMatrix(char c)
  {
    switch (c)
    {
      case 'a' : return 23535;
      case 'b' : return 31727;
      case 'c' : return 29263;
      case 'd' : return 15211;
      case 'e' : return 29391;
      case 'f' : return  4815;
      case 'g' : return 31567;
      case 'h' : return 23533;
      case 'i' : return 29847;
      case 'j' : return 31527;
      case 'k' : return 23277;
      case 'l' : return 29257;
      case 'm' : return 23421;
      case 'n' : return 24573;
      case 'o' : return 31599;
      case 'p' : return  5103;
      case 'q' : return 28527;
      case 'r' : return 22511;
      case 's' : return 31183;
      case 't' : return  9367;
      case 'u' : return 31597;
      case 'v' : return 11117;
      case 'w' : return 24429;
      case 'x' : return 23213;
      case 'y' : return  9709;
      case 'z' : return 29671;
      case '0' : return 31599;
      case '1' : return 29843;
      case '2' : return 29671;
      case '3' : return 31143;
      case '4' : return 18925;
      case '5' : return 31183;
      case '6' : return 31695;
      case '7' : return 18727;
      case '8' : return 31727;
      case '9' : return 31215;
      case '-' : return   448;
      case '+' : return  1488;
      case '\0' : return 0;
      default : return 0;
    }
    return 0;
  }

  // Displays a character (needs its 16-bit int form)
  void printMatrix(uint16_t c, uint8_t x, uint8_t y, uint16_t col)
  {
    for (uint8_t i=0; i<5; i++)
    {
      for (uint8_t j=0; j<3; j++)
      {
        if (bitRead(c, 3*i+j))
          matrix.drawPixel(x + j, y + i, col);
      }
    }
  }

  // Displays a lock in the middle of the screen
  void printLock()
  {
    uint8_t i;
    uint16_t borderColor = matrix.ColorHSV(600, 255, 127, true);
    for (i=0; i<14; i++)
    {
      matrix.drawPixel(9 + i, 16, borderColor);
      matrix.drawPixel(9 + i, 23, borderColor);
    }
    for (i=0; i<8; i++)
    {
      matrix.drawPixel(9, 16 + i, borderColor);
      matrix.drawPixel(22, 16 + i, borderColor);
    }
    for (i=0; i<4; i++)
    {
      matrix.drawPixel(14 + i, 8, borderColor);
      matrix.drawPixel(14 + i, 10, borderColor);
      matrix.drawPixel(10, 12 + i, borderColor);
      matrix.drawPixel(12, 12 + i, borderColor);
      matrix.drawPixel(19, 12 + i, borderColor);
      matrix.drawPixel(21, 12 + i, borderColor);
      matrix.drawPixel(16, 18 + i, borderColor);
    }
    for (i=0; i<2; i++)
    {
      matrix.drawPixel(12 + i, 9, borderColor);
      matrix.drawPixel(18 + i, 9, borderColor);
      matrix.drawPixel(11, 10 + i, borderColor);
      matrix.drawPixel(20, 10 + i, borderColor);
      matrix.drawPixel(15, 18 + i, borderColor);
    }
    matrix.drawPixel(13, 11, borderColor);
    matrix.drawPixel(18, 11, borderColor);
  }

#pragma endregion FUNCTIONS

// Finally, the program in itself !
#pragma region PROGRAM

  // Called at the start of the Arduino
  void setup()
  {
    // Pin initialization
    pinMode(xPin, INPUT);
    pinMode(yPin, INPUT);
    pinMode(buttonPin, INPUT_PULLUP);

    pinMode(okPin, INPUT_PULLUP);
    pinMode(backPin, INPUT_PULLUP);

    // Selections initialization
    selectP = 2;
    selectM = 2;
    selectMenu = 0;
    selectL = 0;
    for (uint8_t i=0; i<6; i++) letterCursors[i] = 0;
    sz = 0;

    // Menu variables initialization (we start on the lock screen)
    selectionChanged = true;
    listChanged = true;
    menuChanged = true;
    currentMenu = MENU_LOCK;

    // Temporary variables initialization
    tmpName = charsToInt("aaa");
    tmpMoney = 0;
    for (uint8_t i=0; i<7; i++) chars[i] = '\0';

    // Memory cursors initialization at first person in alphabetical order
    EEPROM.get<uint8_t>(16, personCursor);
    memCursor = MEM_OFFSET + BLOCK_SIZE * personCursor;

    // RGB Matrix initialization
    matrix.begin();
    matrix.fillScreen(0);

    // Primary colors definition
    baseColor = matrix.ColorHSV(600, 255, 127, true);
    selectColor = matrix.ColorHSV(1200, 255, 127, true);
    otherColor = matrix.ColorHSV(1500, 255, 127, true);

    // Lock screen variables initialization
    unlock = 0;
    lastInput = 0;
  }

  void loop()
  {
    // Reading inputs
    int yJoy = analogRead(yPin);
    int xJoy = analogRead(xPin);
    int ok = digitalRead(okPin);
    int back = digitalRead(backPin);

    // Lock screen : password goes here, so it is hidden within the compilation
    if (currentMenu == MENU_LOCK)
    {
      // As long as we do the same input, don't register another one
      if (lastInput != 0)
      {
        lastInput = 0;
        if (yJoy > 2 * JOYSTICK_MAX / 3) lastInput = 1; // down
        if (yJoy < JOYSTICK_MAX / 3)     lastInput = 2; // up
        if (xJoy > 2 * JOYSTICK_MAX / 3) lastInput = 3; // left
        if (xJoy < JOYSTICK_MAX / 3)     lastInput = 4; // right
        if (ok == IS_PRESSED)            lastInput = 5; // yellow
        if (back == IS_PRESSED)          lastInput = 6; // red
        return;
      }

      if (unlock == 7)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 8; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 0; lastInput = 6;}
      }
      if (unlock == 6)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 7; lastInput = 6;}
      }
      if (unlock == 5)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 6; lastInput = 6;}
      }
      if (unlock == 4)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 5; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 0; lastInput = 6;}
      }
      if (unlock == 3)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 4; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 0; lastInput = 6;}
      }
      if (unlock == 2)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 3; lastInput = 6;}
      }
      if (unlock == 1)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 1; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 2; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 0; lastInput = 6;}
      }
      if (unlock == 0)
      {
        if (yJoy > 2 * JOYSTICK_MAX / 3) {unlock = 1; lastInput = 1;}
        if (yJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 2;}
        if (xJoy > 2 * JOYSTICK_MAX / 3) {unlock = 0; lastInput = 3;}
        if (xJoy < JOYSTICK_MAX / 3)     {unlock = 0; lastInput = 4;}
        if (ok == IS_PRESSED)            {unlock = 0; lastInput = 5;}
        if (back == IS_PRESSED)          {unlock = 0; lastInput = 6;}
      }
      // If we do not have successfully entered the password, don't go further
      if (unlock < 8)
      {
        printLock();
        return;
      }
      // If we do however, go to main menu
      else
      {
        currentMenu = MENU_MAIN;
        menuChanged = true;
        listChanged = true;
        selectionChanged = true;
      }
    }

    // Selection update
    if (analogRead(yPin) > 2 * JOYSTICK_MAX / 3)
    {
      if (currentMenu == MENU_PERSON || currentMenu == MENU_REMOVE)
      {
        if (selectP < 4)
        {
          selectionChanged = true;
          selectP++;
        }
        else listChanged = true;
        EEPROM.get<uint8_t>(memCursor + 7, personCursor);
        memCursor = MEM_OFFSET + BLOCK_SIZE * personCursor;
      }
      if (currentMenu == MENU_ADD)
      {
        listChanged = true;
        letterCursors[selectL] += 1;
        if (letterCursors[selectL] > 26)
          letterCursors[selectL] -= 27;
      }
      if (currentMenu == MENU_CASH)
      {
        if (selectM < 4)
        {
          selectionChanged = true;
          selectM++;
        }
        else listChanged = true;
        tmpMoney++;
      }
      if (currentMenu == MENU_MAIN)
      {
        if (selectMenu < 2)
        {
          selectionChanged = true;
          selectMenu++;
        }
      }
    }
    if (analogRead(yPin) < JOYSTICK_MAX / 3)
    {
      if (currentMenu == MENU_PERSON || currentMenu == MENU_REMOVE)
      {
        if (selectP > 0)
        {
          selectionChanged = true;
          selectP--;
        }
        else listChanged = true;
        EEPROM.get<uint8_t>(memCursor + 6, personCursor);
        memCursor = MEM_OFFSET + BLOCK_SIZE * personCursor;
      }
      if (currentMenu == MENU_ADD)
      {
        listChanged = true;
        letterCursors[selectL] -= 1;
        if (letterCursors[selectL] < 0)
          letterCursors[selectL] += 27;
      }
      if (currentMenu == MENU_CASH)
      {
        if (selectM > 0)
        {
          selectionChanged = true;
          selectM--;
        }
        else listChanged = true;
        tmpMoney--;
      }
      if (currentMenu == MENU_MAIN)
      {
        if (selectMenu > 0)
        {
          selectionChanged = true;
          selectMenu--;
        }
      }
    }

    if (analogRead(xPin) > 2 * JOYSTICK_MAX / 3)
    {
      if (currentMenu == MENU_ADD)
      {
        if (selectL > 0)
        {
          selectL--;
          selectionChanged = true;
        }
      }
    }
    if (analogRead(xPin) < JOYSTICK_MAX / 3)
    {
      if (currentMenu == MENU_ADD)
      {
        if (selectL < 5)
        {
          selectL++;
          selectionChanged = true;
        }
      }
    }

    if (selectionChanged)
    {
      // Print '>' character next to selection
      if (currentMenu == MENU_PERSON || currentMenu == MENU_REMOVE)
      {
        matrix.fillRect(1, 1, 4, 30, 0);
        selectionChanged = false;
        printMatrix(5393, 1, 1 + 6*selectP, selectColor);
      }
      if (currentMenu == MENU_CASH)
      {
        matrix.fillRect(1, 1, 4, 30, 0);
        selectionChanged = false;
        printMatrix(5393, 1, 1 + 6*selectM, selectColor);
      }
      if (currentMenu == MENU_MAIN)
      {
        matrix.fillRect(1, 1, 4, 30, 0);
        selectionChanged = false;
        printMatrix(5393, 1, 1 + 6*selectMenu, selectColor);
      }
      if (currentMenu == MENU_ADD)
      {
        matrix.fillRect(4, 12, 25, 1, 0);
        matrix.fillRect(4, 18, 25, 1, 0);
        for (uint8_t i=0; i<7; i++)
          matrix.fillRect(4 + 4*i, 13, 1, 5, 0);

        selectionChanged = false;

        for (uint8_t i=0; i<5; i++)
        {
          matrix.drawPixel(4 + 4*selectL, 13 + i, otherColor);
          matrix.drawPixel(8 + 4*selectL, 13 + i, otherColor);
          matrix.drawPixel(4 + 4*selectL + i, 12, otherColor);
          matrix.drawPixel(4 + 4*selectL + i, 18, otherColor);
        }
      }
    }
    else
    {
      // Changing menu with the buttons
      if (digitalRead(backPin) == IS_PRESSED)
      {
        if (currentMenu == MENU_MAIN)
        {
          currentMenu = MENU_LOCK;
          unlock = 0;
          matrix.fillScreen(0);
          return;
        }
        if (currentMenu == MENU_PERSON)
        {
          currentMenu = MENU_MAIN;
          listChanged = true;
          selectionChanged = true;
          menuChanged = true;
        }
        if (currentMenu == MENU_ADD)
        {
          currentMenu = MENU_MAIN;
          listChanged = true;
          selectionChanged = true;
          menuChanged = true;
        }
        if (currentMenu == MENU_REMOVE)
        {
          currentMenu = MENU_MAIN;
          listChanged = true;
          selectionChanged = true;
          menuChanged = true;
        }
        if (currentMenu == MENU_CASH)
        {
          currentMenu = MENU_PERSON;
          listChanged = true;
          selectionChanged = true;
          menuChanged = true;
        }
      }
      if (digitalRead(okPin) == IS_PRESSED)
      {
        if (currentMenu == MENU_MAIN)
        {
          if (selectMenu == 0)
          {
            currentMenu = MENU_PERSON;
            listChanged = true;
            selectionChanged = true;
            menuChanged = true;
          }
          if (selectMenu == 1)
          {
            currentMenu = MENU_ADD;
            listChanged = true;
            selectionChanged = true;
            menuChanged = true;
          }
          if (selectMenu == 2)
          {
            currentMenu = MENU_REMOVE;
            listChanged = true;
            selectionChanged = true;
            menuChanged = true;
          }
        }
        else
        {
          if (currentMenu == MENU_CASH)
          {
            EEPROM.put<int>(memCursor + 4, tmpMoney);
          }
          if (currentMenu == MENU_PERSON)
          {
            currentMenu = MENU_CASH;
            listChanged = true;
            selectionChanged = true;
            EEPROM.get<int>(memCursor + 4, tmpMoney);
          }
          if (currentMenu == MENU_ADD)
          {
            // Look at all persons in memory to find prev and next,
            // modify prev's next and next's prev to a new address,
            // and put the new person at this address with prev and next
            // New address is found using the first 16 bytes
            // Don't forget to write the new taken address in these 16 bytes !

            uint8_t newAddr = 127;

            uint64_t freeAddresses;
            EEPROM.get<uint64_t>(8, freeAddresses);
            for (uint8_t i=0; i<64; i++)
            {
              if (bitRead(freeAddresses, i) == 0)
              {
                newAddr = i;
                bitWrite(freeAddresses, i, 1);
                EEPROM.put<uint64_t>(8, freeAddresses);
                break;
              }
            }
            if (newAddr == 127)
            {
              for (uint8_t i=0; i<36; i++)
              {
                if (bitRead(freeAddresses, i) == 0)
                {
                  newAddr = i + 64;
                  bitWrite(freeAddresses, i, 1);
                  EEPROM.put<uint64_t>(8, freeAddresses);
                  break;
                }
              }
            }
            if (newAddr != 127)
            {
              for (uint8_t i=0; i<6; i++) chars[i] = '\0';
              for (uint8_t i=0; i<6; i++)
              {
                if (letterCursors[i] == 0) break;

                chars[i] = 'a' + letterCursors[i] - 1;
              }
              uint32_t name = charsToInt(chars);

              uint16_t cursor;
              uint8_t fst;
              uint8_t next = 127;
              uint32_t otherName;
              EEPROM.get<uint8_t>(MEM_OFFSET - 2, fst);
              cursor = MEM_OFFSET + BLOCK_SIZE * fst;
              uint8_t curr = fst;

              while (next != fst)
              {
                EEPROM.get<uint8_t>(cursor + 7, next);
                EEPROM.get<uint32_t>(MEM_OFFSET + BLOCK_SIZE * next, otherName);

                if (name <= otherName) break;

                cursor = MEM_OFFSET + BLOCK_SIZE * next;
                curr = next;
              }

              if (next == fst)
              {
                EEPROM.get<uint8_t>(cursor + 7, next);
                EEPROM.get<uint32_t>(MEM_OFFSET + BLOCK_SIZE * next, otherName);
              }

              if (name != otherName)
              {
                EEPROM.put<uint8_t>(cursor + 7, newAddr);
                EEPROM.put<uint8_t>(MEM_OFFSET + BLOCK_SIZE * next + 6, newAddr);

                EEPROM.put<uint32_t>(MEM_OFFSET + BLOCK_SIZE * newAddr, name);
                EEPROM.put<int>(MEM_OFFSET + BLOCK_SIZE * newAddr + 4, 0);
                EEPROM.put<uint8_t>(MEM_OFFSET + BLOCK_SIZE * newAddr + 6, curr);
                EEPROM.put<uint8_t>(MEM_OFFSET + BLOCK_SIZE * newAddr + 7, next);

                uint8_t last;
                EEPROM.get<uint8_t>(17, last);

                if (curr == last)
                {
                  if (name < otherName) EEPROM.put<uint8_t>(16, newAddr);
                  else EEPROM.put<uint8_t>(17, newAddr);
                }

                memCursor = MEM_OFFSET + BLOCK_SIZE * newAddr;

                currentMenu = MENU_MAIN;
                listChanged = true;
                selectionChanged = true;
                menuChanged = true;
              }
            }
          }
          if (currentMenu == MENU_REMOVE)
          {
            // prev's next = next, next's prev = prev
            // Update references to first and last if necessary
            // Indicate that the address is free

            uint8_t prev;
            uint8_t next;
            EEPROM.get<uint8_t>(memCursor + 6, prev);
            EEPROM.get<uint8_t>(memCursor + 7, next);
            EEPROM.put<uint8_t>(MEM_OFFSET + BLOCK_SIZE * prev + 7, next);
            EEPROM.put<uint8_t>(MEM_OFFSET + BLOCK_SIZE * next + 6, prev);

            uint8_t addr = (uint8_t) ((memCursor - MEM_OFFSET) / BLOCK_SIZE);
            uint64_t freeAddresses;
            bool firstHalfFree = false;
            EEPROM.get<uint64_t>(8, freeAddresses);
            for (uint8_t i=0; i<64; i++)
            {
              if (bitRead(freeAddresses, i) == 0)
              {
                firstHalfFree = true;
                bitWrite(freeAddresses, addr, 0);
                EEPROM.put<uint64_t>(8, freeAddresses);
                break;
              }
            }
            if (!firstHalfFree)
            {
              bitWrite(freeAddresses, addr - 64, 0);
              EEPROM.put<uint64_t>(0, freeAddresses);
            }

            // prev = first and next = last, because we don't need more variables

            EEPROM.get<uint8_t>(16, prev);
            EEPROM.get<uint8_t>(17, next);

            if (prev == addr)
            {
              uint16_t cursor = MEM_OFFSET + BLOCK_SIZE * prev;
              EEPROM.get<uint8_t>(cursor + 7, prev);
              EEPROM.put<uint8_t>(16, prev);
            }

            if (next == addr)
            {
              uint16_t cursor = MEM_OFFSET + BLOCK_SIZE * next;
              EEPROM.get<uint8_t>(cursor + 6, next);
              EEPROM.put<uint8_t>(17, next);
            }

            memCursor = MEM_OFFSET + BLOCK_SIZE * next;

            listChanged = true;
            selectionChanged = true;
          }
        }
      }
    }

    // If the list display needs to be updated, we do it here
    if (listChanged)
    {
      if (currentMenu != MENU_ADD)
      {
        matrix.fillRect(5, 1, 24, 30, 0);
        listChanged = false;
      }
      else
      {
        matrix.fillRect(5 + 4*selectL, 1, 3, 11, 0);
        matrix.fillRect(5 + 4*selectL, 13, 3, 5, 0);
        matrix.fillRect(5 + 4*selectL, 19, 3, 11, 0);
        listChanged = false;
      }
    }

    // If the menu changed, we need to redraw everything
    if (menuChanged)
    {
      matrix.fillRect(1, 1, 30, 30, 0);
      menuChanged = false;
    }

    // Everything below is the display of the different menus

    // Person selection menus
    if (currentMenu == MENU_PERSON || currentMenu == MENU_REMOVE)
    {
      uint16_t person = memCursor;
      uint16_t color;
      for (uint8_t i=0; i<selectP; i++)
      {
        EEPROM.get<uint8_t>(person + 6, personCursor);
        person = MEM_OFFSET + BLOCK_SIZE * personCursor;
      }
      for (uint8_t i=0; i<5; i++)
      {
        uint32_t name;
        EEPROM.get<uint32_t>(person, name);

        if (person == memCursor) color = selectColor;
        else color = baseColor;

        for (uint8_t j=0; j<NAME_SIZE; j++)
        {
          char c = letter(name, j);
          // The following part may look really stupid.
          // However, if we don't test every single case, EVEN IF THE OUTCOME
          // IS THE SAME, then nothing will appear on screen...
          // The || and && operators won't work either for some obscure reason,
          // same for <, >, <=, >=.
          // Basically, black magic.
          if (c == 'a') printMatrix(charToMatrix('a'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'b') printMatrix(charToMatrix('b'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'c') printMatrix(charToMatrix('c'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'd') printMatrix(charToMatrix('d'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'e') printMatrix(charToMatrix('e'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'f') printMatrix(charToMatrix('f'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'g') printMatrix(charToMatrix('g'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'h') printMatrix(charToMatrix('h'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'i') printMatrix(charToMatrix('i'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'j') printMatrix(charToMatrix('j'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'k') printMatrix(charToMatrix('k'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'l') printMatrix(charToMatrix('l'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'm') printMatrix(charToMatrix('m'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'n') printMatrix(charToMatrix('n'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'o') printMatrix(charToMatrix('o'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'p') printMatrix(charToMatrix('p'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'q') printMatrix(charToMatrix('q'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'r') printMatrix(charToMatrix('r'), 5 + 4*j, 1 + 6*i, color);
          if (c == 's') printMatrix(charToMatrix('s'), 5 + 4*j, 1 + 6*i, color);
          if (c == 't') printMatrix(charToMatrix('t'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'u') printMatrix(charToMatrix('u'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'v') printMatrix(charToMatrix('v'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'w') printMatrix(charToMatrix('w'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'x') printMatrix(charToMatrix('x'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'y') printMatrix(charToMatrix('y'), 5 + 4*j, 1 + 6*i, color);
          if (c == 'z') printMatrix(charToMatrix('z'), 5 + 4*j, 1 + 6*i, color);
        }
        EEPROM.get<uint8_t>(person + 7, personCursor);
        person = MEM_OFFSET + BLOCK_SIZE * personCursor;
      }
    }

    // Cash menu
    if (currentMenu == MENU_CASH)
    {
      uint16_t color;
      char c[16];
      int currentMoney;
      for (uint8_t i=0; i<5; i++)
      {
        for (uint8_t j=0; j<16; j++) c[j] = '\0';

        itoa(tmpMoney - selectM + i, c, 10);

        if (selectM == i) color = selectColor;
        else
        {
          EEPROM.get<int>(memCursor + 4, currentMoney);
          if (currentMoney == tmpMoney - selectM + i)
            color = otherColor;
          else
            color = baseColor;
        }

        for (uint8_t j=0; j<6; j++)
        {
          // Black magic again, don't pay attention
          if (c[j] == '0') printMatrix(charToMatrix('0'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '1') printMatrix(charToMatrix('1'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '2') printMatrix(charToMatrix('2'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '3') printMatrix(charToMatrix('3'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '4') printMatrix(charToMatrix('4'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '5') printMatrix(charToMatrix('5'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '6') printMatrix(charToMatrix('6'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '7') printMatrix(charToMatrix('7'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '8') printMatrix(charToMatrix('8'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '9') printMatrix(charToMatrix('9'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '+') printMatrix(charToMatrix('+'), 5 + 4*j, 1 + 6*i, color);
          if (c[j] == '-') printMatrix(charToMatrix('-'), 5 + 4*j, 1 + 6*i, color);
        }
      }
    }

    // Main menu
    if (currentMenu == MENU_MAIN)
    {
      printMatrix(charToMatrix('l'), 5, 1, baseColor);
      printMatrix(charToMatrix('i'), 9, 1, baseColor);
      printMatrix(charToMatrix('s'), 13, 1, baseColor);
      printMatrix(charToMatrix('t'), 17, 1, baseColor);

      printMatrix(charToMatrix('a'), 5, 7, baseColor);
      printMatrix(charToMatrix('d'), 9, 7, baseColor);
      printMatrix(charToMatrix('d'), 13, 7, baseColor);

      printMatrix(charToMatrix('d'), 5, 13, baseColor);
      printMatrix(charToMatrix('e'), 9, 13, baseColor);
      printMatrix(charToMatrix('l'), 13, 13, baseColor);
      printMatrix(charToMatrix('e'), 17, 13, baseColor);
      printMatrix(charToMatrix('t'), 21, 13, baseColor);
      printMatrix(charToMatrix('e'), 25, 13, baseColor);
    }

    // Person adding menu
    if (currentMenu == MENU_ADD)
    {
      char c;
      uint16_t color;
      for (uint8_t i=0; i<6; i++)
      {
        for (uint8_t j=0; j<5; j++)
        {
          c = letterCursors[i] - 2 + j;
          if (c < 0) c += 27;
          if (c > 26) c -= 27;
          if (c == 0) c = '\0';
          else c += 'a' - 1;

          if (j == 2) color = matrix.ColorHSV(1200, 255, 127, true);
          else color = baseColor;

          if (c == 'a') printMatrix(charToMatrix('a'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'b') printMatrix(charToMatrix('b'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'c') printMatrix(charToMatrix('c'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'd') printMatrix(charToMatrix('d'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'e') printMatrix(charToMatrix('e'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'f') printMatrix(charToMatrix('f'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'g') printMatrix(charToMatrix('g'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'h') printMatrix(charToMatrix('h'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'i') printMatrix(charToMatrix('i'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'j') printMatrix(charToMatrix('j'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'k') printMatrix(charToMatrix('k'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'l') printMatrix(charToMatrix('l'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'm') printMatrix(charToMatrix('m'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'n') printMatrix(charToMatrix('n'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'o') printMatrix(charToMatrix('o'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'p') printMatrix(charToMatrix('p'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'q') printMatrix(charToMatrix('q'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'r') printMatrix(charToMatrix('r'), 5 + 4*i, 1 + 6*j, color);
          if (c == 's') printMatrix(charToMatrix('s'), 5 + 4*i, 1 + 6*j, color);
          if (c == 't') printMatrix(charToMatrix('t'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'u') printMatrix(charToMatrix('u'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'v') printMatrix(charToMatrix('v'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'w') printMatrix(charToMatrix('w'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'x') printMatrix(charToMatrix('x'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'y') printMatrix(charToMatrix('y'), 5 + 4*i, 1 + 6*j, color);
          if (c == 'z') printMatrix(charToMatrix('z'), 5 + 4*i, 1 + 6*j, color);
        }
      }
    }

    // Update every 100 milliseconds
    delay(100);
  }

#pragma endregion PROGRAM
