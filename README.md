# projet ANU

    DESCRIPTION

Ceci est un programme pour Arduino Uno, à faire marcher avec un écran de leds
RGB 32x32 Adafruit, un joystick et deux boutons.

Il s'agit d'un programme qui sert à stocker des noms, et à leur associer un
certain montant, enregistrés dans la ROM de l'Arduino (100 personnes max).

Le tout est protégé par un mot de passe, qui consiste en une série d'inputs à
effectuer sur le joystick et les boutons.

    UTILISATION
    
Joystick : navigation
Bouton 1 : validation
Bouton 2 : retour